import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'exemen2019';
  nbrTotale= 0;

  changeStockValue(event) {
    let s=event+1;
    this.nbrTotale=s;

  }
}
