import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {OffreStageComponent} from './offre-stage/offre-stage.component';


const routes: Routes = [{
  path: 'stagies',
  component: OffreStageComponent,
  pathMatch: 'full'
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
