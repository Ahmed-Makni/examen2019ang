export interface IStage {
    id:number;
    reference:String;
    titre:String;
    description:String;
    expire:boolean;
    duree:String;
    technologie:String;

}
