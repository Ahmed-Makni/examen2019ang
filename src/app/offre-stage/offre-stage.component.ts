import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {StageService} from '../_service/stage.service';
import {IStage} from '../_model/Stage';

@Component({
  selector: 'app-offre-stage',
  templateUrl: './offre-stage.component.html',
  styleUrls: ['./offre-stage.component.css']
})
export class OffreStageComponent implements OnInit {

  @Input() nbrInter:number;
  @Output() event = new EventEmitter();
  constructor(private stageService:StageService) { }

  aff:boolean;
  ok:boolean;

  stages:IStage[];
  ngOnInit() {
    this.aff=false;
    this.ok=false;
    this.getAllStage();
  }
  getAllStage(){
    this.stageService.getStages().subscribe(
        res => {
          this.stages = res;
          console.log(this.stages);
        }, error1 => {
          console.log(error1);
        }
        , () => {
          console.log('Complete');
        }
    )
  }

  onClickConsulter(s, iStage: IStage) {

    this.aff=!this.aff
  }

  onClickInterreser() {

    this.event.emit(this.nbrInter);
  }
}
