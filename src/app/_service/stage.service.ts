import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {IStage} from '../_model/Stage';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StageService {

  private _url:string="/assets/stage.json";
  constructor (private http: HttpClient) {}

  getStages():Observable<IStage[]> {
    return this.http.get<IStage[]>(this._url);
  }
}
